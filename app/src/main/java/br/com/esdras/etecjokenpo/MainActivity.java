package br.com.esdras.etecjokenpo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    final static String GANHOU = "Parabéns, voce venceu!";
    final static String EMPATOU = "Não foi desta vez, deu empate!";
    final static String PERDEU = "Você perdeu, tente outra vez!";

    int escolhaJogador = 0, escolhaComputador = 0;
    TextView textViewResultado;
    ImageView imageViewComputador, imageViewJogador;

    Map<Integer, Integer> mapaImagens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewResultado = findViewById(R.id.textViewResultado);
        imageViewComputador = findViewById(R.id.imageViewComputador);
        imageViewJogador = findViewById(R.id.imageViewJogador);

        mapaImagens = new HashMap<>();
        mapaImagens.put(1, R.drawable.papel);
        mapaImagens.put(2, R.drawable.tesoura);
        mapaImagens.put(3, R.drawable.pedra);
    }

    public void jogar(View view) throws InterruptedException {
        int id = view.getId();
        jogadaComputador();
        jogadaUsuario(id);

        if (escolhaJogador == OpcoesEnum.PAPEL.getId()) {
            if (escolhaComputador == OpcoesEnum.PEDRA.getId()) {
                textViewResultado.setText(GANHOU);
            } else {
                if (escolhaComputador == OpcoesEnum.TESOURA.getId()) {
                    textViewResultado.setText(PERDEU);
                } else {
                    textViewResultado.setText(EMPATOU);
                }
            }
        }

        if (escolhaJogador == OpcoesEnum.TESOURA.getId()) {
            if (escolhaComputador == OpcoesEnum.PAPEL.getId()) {
                textViewResultado.setText(GANHOU);
            } else {
                if (escolhaComputador == OpcoesEnum.PEDRA.getId()) {
                    textViewResultado.setText(PERDEU);
                } else {
                    textViewResultado.setText(EMPATOU);
                }
            }
        }

        if (escolhaJogador == OpcoesEnum.PEDRA.getId()) {
            if (escolhaComputador == OpcoesEnum.TESOURA.getId()) {
                textViewResultado.setText(GANHOU);
            } else {
                if (escolhaComputador == OpcoesEnum.PAPEL.getId()) {
                    textViewResultado.setText(PERDEU);
                } else {
                    textViewResultado.setText(EMPATOU);
                }
            }
        }
    }

    private void jogadaUsuario(int id) {
        switch (id) {
            case R.id.imageViewPapel:
                escolhaJogador = 1;
                break;
            case R.id.imageViewTesoura:
                escolhaJogador = 2;
                break;
            case R.id.imageViewPedra:
                escolhaJogador = 3;
                break;
        }
        imageViewJogador.setImageResource(mapaImagens.get(escolhaJogador));
    }

    private void jogadaComputador() {
        Random random = new Random();
        escolhaComputador = random.nextInt(3) + 1;
        imageViewComputador.setImageResource(mapaImagens.get(escolhaComputador));
    }
}

enum OpcoesEnum {
    PAPEL(1), TESOURA(2), PEDRA(3);

    private int id;

    public int getId() {
        return id;
    }

    OpcoesEnum(int id) {
        this.id = id;
    }
}
